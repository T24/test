# 2020软件工程作业03

| 软件工程作业 | https://edu.cnblogs.com/campus/zswxy/2018SE                  |
| ------------ | ------------------------------------------------------------ |
| **作业要求** | https://edu.cnblogs.com/campus/zswxy/2018SE/homework/11354   |
| **作业选题** | **仿写《阿里云APP》----考试系统**                            |
| **作业目标** | **熟悉需求分析、原型设计、以及软件开发流程**                 |
| **原型工具** | **墨刀** https://modao.cc/app/1854a541f26ae44248a008672502e242a8d52508?simulator_type=device&sticky |
| **码云地址** | https://gitee.com/T24/test                                   |

## 需求分析——“NABCD模型”

（1）Need，需求

> ​         教育一直都是人们所关注且十分重视的话题。在当前社会飞速发展以及经济水平不断提升的过程中，大家对思想层面的东西越来越注重，因此教育的发展被广泛关注着。而在教育行业中大家往往更习惯通过考试对一个阶段的学习成果进行检验。在线考试系统在工作效率以及便捷性方面展现了巨大优势。  

（2）Approach，做法

> - 在网上推出测试版，并且请用户填写用户体验，对于有什么地方需要改进给些建议。
> - 教师可以登录APP查看学生的成绩以及进行导入试题和阅卷等操作。
> - 学生可以登录APP进行线上测试、在线练习以及看网课等操作。

（3）Benfit，好处

> - 对于教师来说，他们能够对进行在线导入试题、阅卷以及查看学生对知识的掌握程度，可以节省很多精力。
> - 对于学生来说，他们能够同时进行观看各名校的网课、进行在线练习和考试，免去了下载多个app的麻烦。

4.可以根据实际需要进行试题、试卷的添加删除等的操作。

（4）Competitors，竞争

> -    可以请先把软件给学校或者教师学生试用，这样可以得到相对真实的用户体验，在此基础上对app进行修改和完善。
> -  页面简介流畅，操作更快捷方便 。  

（5）Delivery，推广

>   利用一切可以利用的廉价的广告资源，一开始给人们高的利益，收取少量的中介费。 

## 原型设计

- **功能设计**

  <img src="E:\22.png" alt="22" style="zoom:50%;" />

  

  <img src="E:\33.png" alt="33" style="zoom:50%;" />

  <img src="E:\55.png" alt="55" style="zoom: 33%;" />

- **界面设计**

  ​     首先用户可以通过手机号进行注册或者直接通过第三方应用登录到APP。

<img src="C:\Users\屁滕\AppData\Roaming\Typora\typora-user-images\1602932325430.png" alt="1602932325430" style="zoom:50%;" />

​            然后进入到首页，学生用户可以进行线上考试、培训以及查看自己的答题记录等操作。

<img src="C:\Users\屁滕\AppData\Roaming\Typora\typora-user-images\1602934962949.png" alt="1602934962949" style="zoom:50%;" />

​         该页面学生用户能看到自己选择的考试科目以及该科目的考试倒计时。此时学生需要在系统显示的规定时间内将全部试题内容予以完成，如若系统时间结束则自动跳到0秒，此时所有试题将自动提交数据库，此时考生无法继续作答。

<img src="E:\屏幕截图 2020-10-17 232322.png" alt="屏幕截图 2020-10-17 232322" style="zoom:50%;" />

​            在“资料”模块下，通过调用数据库的数据用户可以进行随机测试、练习以及观看各种公开课和资料文件。当然也可以进行关键词搜索。

<img src="C:\Users\屁滕\AppData\Roaming\Typora\typora-user-images\1602934999944.png" alt="1602934999944" style="zoom:50%;" />

​         此页面下用户可以对自己的个人信息进行修改为的是让用户自己的账号登陆安全性更高而提供的操作。点击个人信息按钮获取相对应的JSP界面再请求服务端，和服务端达到交互。当完成信息修改操作以后此时系统用户登入状态将自动注销之前的信息如密码并提示需要重新登录。除此之外，用户还可以将自己的学习码分享给好友。了解APP版本信息等。  

<img src="C:\Users\屁滕\AppData\Roaming\Typora\typora-user-images\1602935022900.png" alt="1602935022900" style="zoom:50%;" />